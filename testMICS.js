
const MICS_VZ_89TE = require('./classes/MICS_VZ_89TE.js').default


const interval = setInterval(() => { 
    MICS_VZ_89TE.write();
    var timeout = setTimeout(() => {
        MICS_VZ_89TE.read();
        if(MICS_VZ_89TE.getStatus() == 0x00) {
            Log.log("read buffer", MICS_VZ_89TE.getReadBuffer())
            Log.log(MICS_VZ_89TE.getTVOC(), MICS_VZ_89TE.getCO2(), MICS_VZ_89TE.getResistance(), MICS_VZ_89TE.checkCRC(MICS_VZ_89TE.getReadBuffer()))
            Log.log("read crc", MICS_VZ_89TE.calcCRC(MICS_VZ_89TE.getReadBuffer()))
        }
    }, 50)
}, 2000);