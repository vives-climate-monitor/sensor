const Apa102spi = require('apa102-spi')
require('dotenv').config()

// Sensoren initialiseren
const HIH6030 = require('./classes/HIH6030').default
const iAQCore = require('./classes/iAQCore').default
const VEML7700 = require('./classes/VEML7700').default
// file log helper
const File = require('./classes/File').default
// web server && request  helper
const Http = require('./classes/Http').default
// config helper
const Config = require('./classes/config')
const Logger = require('./classes/Loggers/Logger')
const Log = require('./classes/consoleLog');
const RGB_Leds = require('./classes/RGB_leds');


var config;
const PORT = process.env.PORT || 3000;
const LEDS = 17
var temperatureOffset = 0
var co2Offset = 0
var humidityOffset = 0
var tvocOffset = 0

var co2ArrayMean = []
var tempArrayMean = []
var humArrayMean = []
var tvocArrayMean = []
const refreshTimeSaveInMin = 5
const refreshTimeWebsiteInSec = 1
const refreshTimeLedsInMs = 200

var startupMode = true
var errorCounter = 0
var errorMode = false

const refreshWebsiteTimeout = setInterval(async () => {
  Leds = await RGB_Leds.getInstance();
  updateConfig();
  const data = await messureData()
  if (data != null) {
    // update leds
    Leds.updateSensorValues(data)
    // update websites
    Http.io.emit('co2 messure', data.co2)
    Http.io.emit('temp messure', data.temp)
    Http.io.emit('hum messure', data.hum)
    Http.io.emit('tvoc messure', data.tvoc)
    Http.io.emit('time messure', data.time)
    // add data to list to calculate the mean
    co2ArrayMean.push(data.co2)
    tempArrayMean.push(data.temp)
    humArrayMean.push(data.hum)
    tvocArrayMean.push(data.tvoc)
  }
}, refreshTimeWebsiteInSec * 1000)

const refreshLedsTimeout = setInterval(async () => {
  Leds = await RGB_Leds.getInstance();
  Leds.determineLedColor();
}, refreshTimeLedsInMs)

const saveTimeout = setInterval(async () => {
  // calculate the mean of the last 5 minutes
  let sum = 0
  for (var value in co2ArrayMean) {
    sum += parseInt(co2ArrayMean[value])
  }
  const co2Mean = Math.round(sum / co2ArrayMean.length)
  co2ArrayMean = []

  sum = 0
  for (var value in tempArrayMean) {
    sum += parseInt(tempArrayMean[value])
  }
  const tempMean = Math.round(sum / tempArrayMean.length * 10) / 10
  tempArrayMean = []

  sum = 0
  for (var value in humArrayMean) {
    sum += parseInt(humArrayMean[value])
  }
  const humMean = Math.round(sum / humArrayMean.length)
  humArrayMean = []

  sum = 0
  for (var value in tvocArrayMean) {
    sum += parseInt(tvocArrayMean[value])
  }
  const tvocMean = Math.round(sum / tvocArrayMean.length)
  tvocArrayMean = []
  // save to file

  const coeff = 1000 * 60 * refreshTimeSaveInMin
  const dateToRound = new Date() // or use any other date
  const data = {
    co2: co2Mean,
    temp: tempMean,
    hum: humMean,
    tvoc: tvocMean,
    time: new Date(Math.floor(dateToRound.getTime() / coeff) * coeff)
  }

  File.append(data)
  const logger = await Logger.getInstance()
  logger.log(data)
}, refreshTimeSaveInMin * 60 * 1000)

function messureData() {
  return new Promise(async (resolve) => {
    Leds = await RGB_Leds.getInstance();
    let data = null
    // read new data
    iAQCore.read()
    HIH6030.write()
    VEML7700.startSensor()
    return setTimeout(() => {
      VEML7700.read() // after reading, the sensor turns off.
      HIH6030.read()
      // check if data is correct
      if (HIH6030.getStatus() == 0 && iAQCore.getStatus() == 0) {
        errorCounter = 0
        if (errorMode) {  // error mode is over
          Log.log(Log.types.INFO, `All sensors are working again`)
          errorMode = false
          Leds.is_error = false;
        }
        if (startupMode) {  // startup mode is over
          startupMode = false
          Leds.is_startup = false;
          Log.log(Log.types.INFO, `All sensors ready`)
        }

        const co2 = iAQCore.getCO2() + co2Offset
        const temp = Math.round((HIH6030.getTemp() + temperatureOffset) * 10) / 10
        const hum = Math.round(HIH6030.getHum() + humidityOffset)
        const tvoc = iAQCore.getTVOC() + tvocOffset;
        const dateNow = new Date()
        const time = dateNow.toUTCString()
        data = {
          co2,
          temp,
          hum,
          tvoc,
          time
        }
      } else {
        errorCounter++;
        if (errorCounter >= 2 && startupMode == false) {
          errorMode = true;
          Leds.is_error = true;
          if (HIH6030.getStatus() != 0) {
            Log.log(Log.types.ERROR, `Failed to read sensor data: statusbit HIH6030 = ${HIH6030.getStatus()}`)
          }
          if (iAQCore.getStatus() != 0) {
            Log.log(Log.types.ERROR, `Failed to read sensor data: statusbit iAQCore = ${iAQCore.getStatus()}`)
          }
        }
        else if (HIH6030.getStatus() != 0 && !startupMode) {
          Log.log(Log.types.WARNING, `statusbit HIH6030 = ${HIH6030.getStatus()}`)
        }
        if (iAQCore.getStatus() != 0 && !startupMode) {
          Log.log(Log.types.WARNING, `statusbit iAQCore = ${iAQCore.getStatus()}`)
        }
        data = null
      }
      resolve(data)
    }, 200)
  })
}

async function updateConfig() {
  config = await Config.getInstance();
  Leds = await RGB_Leds.getInstance();
  config.setupFromJSonFile();

  temperatureOffset = config.tempOffset || 0;
  co2Offset = config.co2Offset || 0;
  humidityOffset = config.humOffset || 0;
  tvocOffset = config.tvocOffset || 0;

  Leds.updateVarsFromConfig();
}

Http.http.listen(PORT, async () => {
  Leds = await RGB_Leds.getInstance();
  Log.log(Log.types.INFO, `Sensor server is running on port ${PORT}`)
  Leds.turnLedsOff();
})
