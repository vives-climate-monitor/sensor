FROM arm32v7/ubuntu:18.04
USER root

RUN apt-get update \
    && apt-get -y install curl gnupg build-essential  \
    && curl -sL https://deb.nodesource.com/setup_14.x  | bash - \
    && apt-get -y install nodejs \
    && apt-get autoclean -y \
    && apt-get autoremove -y

WORKDIR /src
COPY . /src

RUN npm install --production

EXPOSE 3000

CMD npm start
