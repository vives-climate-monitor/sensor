const i2c = require('i2c-bus')
const Log = require('./consoleLog');

class MICS_VZ_89TE {
    constructor() {
        this.readBuffer = Buffer.alloc(7)
        this.address = 0x70
        Log.log(Log.types.INFO, `MICS_VZ_89TE helper created!`)
    }

    read() {
        const i2cMICS = i2c.openSync(1)
        i2cMICS.i2cReadSync(this.getAddress(), this.getReadBuffer().length, this.getReadBuffer())
        i2cMICS.closeSync()
    }

    write() {
        var cmd = 0x0c
        var writeBuffer = Buffer.from([cmd, 0, 0, 0, 0, 0])
        const i2cMICS = i2c.openSync(1)
        i2cMICS.i2cWriteSync(this.getAddress(), writeBuffer.length, writeBuffer);
        i2cMICS.closeSync()
    }

    calcCRC(buffer) {
        var sum = 0
        for(var x = 0; x < buffer.length - 1; x++) {
            sum += buffer[x];
        }
        sum >>= 4
        sum += (sum / 0x100);
        return 0xff - parseInt(sum);
    }

    checkCRC(buffer) {
        return (this.calcCRC(buffer) == buffer[-1])
    }

    getReadBuffer() {
        return this.readBuffer
    }

    getAddress() {
        return this.address
    }

    getCO2() {
        const value = (this.getReadBuffer()[1] - 13) * (1600 / 229) + 400;
        return value
    }

    getTVOC() {
        const value = (this.getReadBuffer()[0] - 13) * (1000 / 229);
        return value
    }

    getResistance() {
        const value = (this.getReadBuffer()[4] + 256 * this.getReadBuffer()[3]) + (65536 * this.getReadBuffer()[2]);
        return value
    }

    getStatus() {
        return this.getReadBuffer()[5];
    }

//  VOCvalue = (data[0] - 13) * (1000 / 229);
//  CO2value = (data[1] - 13) * (1600 / 229) + 400;
//  ResistorValue = 10 * (data[4] + (256 * data[3]) + (65536 * data[2]));
}

exports.default = new MICS_VZ_89TE()
