const express = require('express')
const Log = require('./consoleLog');

const app = express()
const http = require('http').createServer(app)
const io = require('socket.io')(http)
const path = require('path')

const File = require('./File').default

class Http {
    constructor() {
        this.http = http
        this.io = io;
        // This is for the public folder on path /
        app.use(express.static(path.join(__dirname, 'public')))
    }
}

app.get('/', (req, res) => {
    // with database => get data first
    res.sendFile(path.join(__dirname, 'public', 'home.html'))
    // updateData();
})

app.get('/getData/:range', (req, res) => {
    const dateNow = new Date()
    const data = File.read()
    const newData = {}

    for (const element in data) {
        let day = new Date(data[element].time)
        // make a new array without objects older then the range (Hours)
        if (dateNow.getTime() - day.getTime() < parseInt(req.params.range) * 60 * 60 * 1000) {
            newData[data[element].time] = data[element]
        }
        day = null
    }
    res.send(newData)
})

exports.default = new Http()
