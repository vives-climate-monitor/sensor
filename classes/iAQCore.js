const i2c = require('i2c-bus')
const Log = require('./consoleLog');

class iAQCore {
    constructor() {
        this.buffer = Buffer.alloc(9)
        this.address = 0x5a
        Log.log(Log.types.INFO, `iAQCore helper created!`)
    }

    read() {
        const i2cIAQ = i2c.openSync(1)
        i2cIAQ.i2cReadSync(this.getAddress(), this.getBuffer().length, this.getBuffer())
        i2cIAQ.closeSync()
    }

    getBuffer() {
        return this.buffer
    }

    getAddress() {
        return this.address
    }

    getStatus() {
        return parseInt(this.buffer.slice(2, 4).readUInt16BE() & 0xff00)
    }

    getCO2() {
        const value = this.buffer.slice(0, 2).readUInt16BE()
        return (value > 2000) ? 2000 : value
    }

    getResistance() {
        return (this.buffer.slice(4, 8).readUInt32BE() & 0xffffff00) >> 8
    }

    getTVOC() {
        return this.buffer.slice(7, 9).readUInt16BE()
    }
}

exports.default = new iAQCore()
