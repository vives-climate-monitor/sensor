const fs = require('fs')
const path = require('path')
const Log = require('./consoleLog');

class Config {
    constructor() {
        Log.log(Log.types.INFO, `initialising from data/config.json`)
        if (fs.existsSync(path.join(__dirname, '../data/config.json')))
            this.setupFromJSonFile()
    }

    setupFromJSonFile() {
        const data = fs.readFileSync(path.join(__dirname, '../data/config.json'))
        Object.assign(this, JSON.parse(data))
    }
    

    static async getInstance() {
        if (!Config.instance) {
            Config.instance = await new Config()
        }
        return Config.instance
    }
}

module.exports = Config
