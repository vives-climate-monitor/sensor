const fs = require('fs')
const path = require('path')
const Apa102spi = require('apa102-spi')
const hexRgb = require('hex-rgb');

const Log = require('./consoleLog');
const Config = require('./config')
const VEML7700 = require('./VEML7700').default;

class RGB_Leds {

    constructor() {
        this.max_brightness = 15;
        this.number_of_leds = 5; // 3 rows of 5 leds (cannot control a seperate row)
        this.max_light = 3000;  // value light sensor measures when brightness needs to be maximum
        this.treshold_darkness = 1;   // this limit determines when leds turns off when dark
        this.is_startup = true;
        this.is_error = false;
        this.sensor_limits = { co2: { min: 450, max: 2000 }, tvoc: { min: 125, max: 600 }, temp: { min: 10, max: 35 }, hum: { min: 15, max: 70 } }
        this.gradiant_percentage = 1;   // value between 0 and 1 => tells where leds are between start and stop color.
        this.gradiant_speed = 0.025; // speed of color sweep 

        this.sensor_values = { co2: 450, tvoc: 125, temp: 10, hum: 15 };

        this.Led_driver = new Apa102spi(this.number_of_leds, 100);

        this.getBrightness();
        this.updateVarsFromConfig()
    }

    async updateVarsFromConfig() {
        const config = await Config.getInstance();
        this.leds_depending_on = config.ledColorDependingOn;
        this.leds_on_when_dark = config.ledsInDarkOn;
        this.colors_gradiants = config.colorGradiants;
        this.change_brightness_manually = config.changeBrightnessManually;
        this.manual_brightness = config.manualBrightness;
    }

    getBrightness() {
        if(this.change_brightness_manually) {
            this.brightness = this.manual_brightness;
        } else {
            this.brightness = VEML7700.getLight() / this.max_light * this.max_brightness;
            this.brightness = (this.brightness > this.max_brightness) ? this.max_brightness : this.brightness;
            this.brightness = (!this.leds_on_when_dark && this.brightness <= this.treshold_darkness) ? 0 : this.brightness;
        }
    }

    updateSensorValues(data) {
        this.sensor_values = data;
    }

    determineLedColor() {
        const color = { red: 0, green: 0, blue: 0 }
        this.getBrightness();

        if ((this.leds_depending_on != "other") && !this.is_error && !this.is_startup) {
            let val = this.leds_depending_on;
            var percentage = (this.sensor_values[val] - this.sensor_limits[val].min) / (this.sensor_limits[val].max - this.sensor_limits[val].min)
            var rgb_min = hexRgb(this.colors_gradiants[val].min);
            var rgb_max = hexRgb(this.colors_gradiants[val].max);

            color.red = Math.round(percentage * (rgb_max.red - rgb_min.red) + rgb_min.red);
            color.green = Math.round(percentage * (rgb_max.green - rgb_min.green) + rgb_min.green);
            color.blue = Math.round(percentage * (rgb_max.blue - rgb_min.blue) + rgb_min.blue);
        } else {
            var rgb_min = hexRgb(this.colors_gradiants.other.min);
            var rgb_max = hexRgb(this.colors_gradiants.other.max);

            this.gradiant_percentage += this.gradiant_speed;

            if(this.gradiant_percentage >= 1) {
                this.gradiant_percentage = 1;
                this.gradiant_speed *= -1;
            } else if(this.gradiant_percentage <= 0){
                this.gradiant_percentage = 0;
                this.gradiant_speed *= -1;
            }

            color.red = Math.round(this.gradiant_percentage * (rgb_max.red - rgb_min.red) + rgb_min.red);
            color.green = Math.round(this.gradiant_percentage * (rgb_max.green - rgb_min.green) + rgb_min.green);
            color.blue = Math.round(this.gradiant_percentage * (rgb_max.blue - rgb_min.blue) + rgb_min.blue);            
        }
        for (let x = 0; x <= this.number_of_leds; x++) {
            this.Led_driver.setLedColor(x, this.brightness, color.red, color.green, color.blue)
        }
        this.Led_driver.sendLeds()
    }
    
    turnLedsOff() {
        for (let x = 0; x <= this.number_of_leds; x++) {
            this.Led_driver.setLedColor(x, 1, 0, 0, 0)
        }
        this.Led_driver.sendLeds()
    }

    static async getInstance() {
        if (!RGB_Leds.instance) {
            RGB_Leds.instance = await new RGB_Leds()
        }
        return RGB_Leds.instance
    }
}

module.exports = RGB_Leds
