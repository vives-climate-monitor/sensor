const fs = require('fs')
const path = require('path')
const moment = require('moment')
const Log = require('./consoleLog');

class File {
    constructor() {
        if (!fs.existsSync(path.join(__dirname, 'data'))) {
            Log.log(Log.types.INFO, `making data directory`)
            fs.mkdirSync(path.join(__dirname, 'data'))
        }
    }

    clean() {
        const date = moment().day(-7).format('yyyy-MM-DD')

        // delete 7 day old file if exists
        if (fs.existsSync(path.join(__dirname, `data/${date}.txt`))) {
            fs.unlinkSync(path.join(__dirname, `data/${date}.txt`))
        }
    }

    append(data) {
        this.clean()
        const date = moment().format('yyyy-MM-DD')

        // will create file if not exists
        fs.appendFileSync(path.join(__dirname, `data/${date}.txt`), `${JSON.stringify(data)}\n`)
    }

    read() {
        // get required files, from oldest => newest
        const files = []
        for (let i = 6; i >= 0; i--) {
            const date = moment().subtract(i, "days").format('yyyy-MM-DD')
            files.push(`data/${date}.txt`)
        }

        try {
        // read all files & parse data
            const data = []
            files.forEach(file => {
                // only read if file exists
                if (fs.existsSync(path.join(__dirname, file))) {
                    const raw = fs.readFileSync(path.join(__dirname, file))
                    const entries = raw.toString().split('\n')
                    data.push.apply(data, entries.slice(0, -1).map(entry => JSON.parse(entry)))
                }
            })
            return data
        } catch (e) {
            Log.log(Log.types.ERROR, 'Something went wrong while trying to read the data')
        }
    }
}

exports.default = new File()
