const knex = require('knex')
const Config = require('../config')
const Log = require('../consoleLog');

class MysqLlogger {
    constructor() {
        this.init();
    }

    async init() {
        const config = await Config.getInstance()
        if(config["exportDatabaseSettings"] && config["exportDatabaseSettings"]["mysqlEnabled"]){
            this.table = config["exportDatabaseSettings"]["mysqlTable"];
            this.knex = knex({
                client: 'mysql',
                connection: {
                  host: config["exportDatabaseSettings"]["mysqlHost"],
                  port: config["exportDatabaseSettings"]["mysqlPort"],
                  user: config["exportDatabaseSettings"]["mysqlUser"],
                  password: config["exportDatabaseSettings"]["mysqlPassword"],
                  database: config["exportDatabaseSettings"]["mysqlDatabase"],
                }
              });
            return this.isEnabled = true
        }
        this.isEnabled = false
    }

    // Table vives-climate-monitor_table, collums: temperature, humidity, tvoc, co2
    log(data) {
        const temp = data.temp
        const hum = data.hum
        const tvoc = data.tvoc
        const co2 = data.co2
        const time = data.time
        if(this.table) {
            return  this.knex.table(this.table)
                .insert({temperature:temp, humidity:hum, tvoc:tvoc, co2:co2, time:time})
                .then(Log.log(Log.types.INFO, `row inserted in database`))
                .catch(err => {
                        Log.log(Log.types.ERROR, `error pushing data to database \n\r\t${err}`)
                });
        }
    }
    

    static async getInstance() {
        if (!ConMysqLloggerfig.instance) {
            MysqLlogger.instance = await new MysqLlogger()
        }
        return MysqLlogger.instance
    }
}

module.exports = MysqLlogger
