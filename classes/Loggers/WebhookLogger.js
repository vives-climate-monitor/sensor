const axios =  require('axios')
const Config = require('../config')
const Log = require('../consoleLog');

class WebhookLogger {
    constructor() {
        this.init();
    }

    async init() {
        const config = await Config.getInstance()
        if(config["exportWebhookSettings"] && config["exportWebhookSettings"]["webhookEnabled"]){
            this.url = config["exportWebhookSettings"]["webhookURL"]
            return this.isEnabled = true
        }

        this.isEnabled = false
    }

    log(data) {
        if(this.isEnabled)
            return axios.post(this.url, data)
            .then(response => {
                Log.log(Log.types.INFO, response)
            })
            .catch(err => Log.log(Log.types.ERROR, `failed to log to webhook: ${err.message}`))
    }
    

    static async getInstance() {
        if (!ConWebhookLoggerfig.instance) {
            WebhookLogger.instance = await new WebhookLogger()
        }
        return WebhookLogger.instance
    }
}

module.exports = WebhookLogger
