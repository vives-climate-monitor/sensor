const WebhookLogger = require('./WebhookLogger')
const MySQLLogger = require('./MySQLLogger')
const Log = require('../consoleLog');

class Logger {
    constructor() {
        this.loggers = [new MySQLLogger() ,new WebhookLogger()].filter(l => !l.isEnabled)
    }

    log(data) {
       return this.loggers.forEach(logger => logger.log(data))
    }
    

    static async getInstance() {
        if (!Logger.instance) {
            Logger.instance = await new Logger()
        }
        return Logger.instance
    }
}

module.exports = Logger
